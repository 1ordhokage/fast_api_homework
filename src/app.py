from fastapi import FastAPI
from passlib.handlers.pbkdf2 import pbkdf2_sha256

from src.api.base_router import router
from src.core.settings import settings
from src.db.db import Session
from src.models.schemas.user.roles import Roles
from src.models.user import User


tags_dict = [
    {
        'name': 'users',
        'description': 'User handlers.'
    },
    {
        'name': 'products',
        'description': 'Product handlers.'
    },
    {
        'name': 'tanks',
        'description': 'Tank handlers.'
    },
    {
        'name': 'operations',
        'description': 'Operation handlers.'
    },
    {
        'name': 'files',
        'description': 'Csv-file handlers.'
    }
]

app = FastAPI(
    title='Homework fastapi app',
    description='Web API CRUD.',
    version='0.0.1',
    openapi_tags=tags_dict
)


@app.on_event('startup')
def user_on_startup(session: Session = Session()):
    users = (
        session
        .query(User)
        .filter(User.role == Roles.admin.value)
        .order_by(
            User.id.desc()
        )
        .all()
    )
    if not users:
        user = User(
            username=settings.admin_login,
            password_hashed=pbkdf2_sha256.hash(settings.admin_password),
            role=Roles.admin.value,
        )
        session.add(user)
        session.commit()
        return user
    return None


app.include_router(router)
