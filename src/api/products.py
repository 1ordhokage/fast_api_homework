from fastapi import APIRouter, Depends, status

from src.models.schemas.product.product_request import ProductRequest
from src.models.schemas.product.product_response import ProductResponse
from src.services.products import ProductsService
from src.services.users import get_current_user_id


router = APIRouter(
    prefix='/products',
    tags=['products'],
)


@router.post('/create', response_model=ProductResponse, status_code=status.HTTP_201_CREATED, name='Product creation')
def create_product(product_schema: ProductRequest, product_service: ProductsService = Depends(), super_user_id: int = Depends(get_current_user_id)):
    return product_service.add_product(product_schema, super_user_id)


@router.get('/all', response_model=list[ProductResponse], name='Get all products')
def get_all_products(product_service: ProductsService = Depends(), super_user_id: int = Depends(get_current_user_id)):
    return product_service.get_all_products()


@router.get('/get/{product_id}', response_model=ProductResponse, name='Get product by id')
def get_product_by_id(product_id: int, product_service: ProductsService = Depends(), super_user_id: int = Depends(get_current_user_id)):
    return product_service.get_product_by_id(product_id)


@router.put('/update/{product_id}', response_model=ProductResponse, name='Update product')
def update_product(product_id: int, product_schema: ProductRequest, product_service: ProductsService = Depends(), super_user_id: int = Depends(get_current_user_id)):
    return product_service.update_product(product_id, product_schema, super_user_id)


@router.delete('/delete/{product_id}', status_code=status.HTTP_204_NO_CONTENT, name='Delete product')
def delete_product(product_id: int, product_service: ProductsService = Depends(), super_user_id: int = Depends(get_current_user_id)):
    return product_service.delete_product(product_id)
