from fastapi import APIRouter, Depends, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm

from src.models.schemas.user.user_request import UserRequest, UserUpdate
from src.models.schemas.user.user_response import UserResponse
from src.models.schemas.utils.jwt_token import JwtToken

from src.services.users import UsersService, get_current_user_id


router = APIRouter(
    prefix='/users',
    tags=['users'],
)


@router.post('/register', status_code=status.HTTP_201_CREATED, name='Registration')
def register(username: str, password_text: str, user_service: UsersService = Depends()):
    return user_service.register(username, password_text)


@router.post('/authorize', response_model=JwtToken, name='Authorization')
def authorize(
    auth_schema: OAuth2PasswordRequestForm = Depends(),
    users_service: UsersService = Depends()
):
    result = users_service.authorize(auth_schema.username, auth_schema.password)
    if not result:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail='Not authorized')
    return result


@router.post('/create', response_model=UserResponse, status_code=status.HTTP_201_CREATED, name='User creation')
def create_user(user_schema: UserRequest, user_service: UsersService = Depends(), super_user_id: int = Depends(get_current_user_id)):
    user_service.check_permission(super_user_id)
    return user_service.add_user(user_schema, super_user_id)


@router.get('/all', response_model=list[UserResponse], name='Get all users')
def get_all_users(user_service: UsersService = Depends(), super_user_id: int = Depends(get_current_user_id)):
    user_service.check_permission(super_user_id)
    return user_service.get_all_users()


@router.get('/get/{user_id}', response_model=UserResponse, name='Get user by id')
def get_user_by_id(user_id: int, user_service: UsersService = Depends(), super_user_id: int = Depends(get_current_user_id)):
    user_service.check_permission(super_user_id)
    return user_service.get_user_by_id(user_id)


@router.put('/update/{user_id}', response_model=UserResponse, name='Update user')
def update_user(user_id: int, user_schema: UserUpdate, user_service: UsersService = Depends(), super_user_id: int = Depends(get_current_user_id)):
    user_service.check_permission(super_user_id)
    return user_service.update_user(user_id, user_schema, super_user_id)


@router.delete('/delete/{user_id}', status_code=status.HTTP_204_NO_CONTENT, name='Delete user')
def delete_user(user_id: int, user_service: UsersService = Depends(), super_user_id: int = Depends(get_current_user_id)):
    user_service.check_permission(super_user_id)
    return user_service.delete_user(user_id)
