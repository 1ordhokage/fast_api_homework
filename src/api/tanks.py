from fastapi import APIRouter, Depends, status

from src.models.schemas.tank.tank_request import TankRequest, TankUpdate
from src.models.schemas.tank.tank_response import TankResponse
from src.services.tanks import TanksService
from src.services.users import get_current_user_id


router = APIRouter(
    prefix='/tanks',
    tags=['tanks'],
)


@router.post('/create', response_model=TankResponse, status_code=status.HTTP_201_CREATED, name='Tank creation')
def create_tank(tank_schema: TankRequest, tank_service: TanksService = Depends(), super_user_id: int = Depends(get_current_user_id)):
    return tank_service.add_tank(tank_schema, super_user_id)


@router.get('/all', response_model=list[TankResponse], name='Get all tanks')
def get_all_tanks(tank_service: TanksService = Depends(), super_user_id: int = Depends(get_current_user_id)):
    return tank_service.get_all_tanks()


@router.get('/get/{tank_id}', response_model=TankResponse, name='Get tank by id')
def get_tank_by_id(tank_id: int, tank_service: TanksService = Depends(), super_user_id: int = Depends(get_current_user_id)):
    return tank_service.get_tank_by_id(tank_id)


@router.get('/update_current_capacity/{tank_id}', response_model=TankResponse, name='Update tank current capacity')
def update_tank(tank_id: int, current_capacity: float, tank_service: TanksService = Depends(), super_user_id: int = Depends(get_current_user_id)):
    return tank_service.update_capacity(tank_id, current_capacity, super_user_id)


@router.put('/update/{tank_id}', response_model=TankResponse, name='Update tank')
def update_tank(tank_id: int, tank_schema: TankUpdate, tank_service: TanksService = Depends(), super_user_id: int = Depends(get_current_user_id)):
    return tank_service.update_tank(tank_id, tank_schema, super_user_id)


@router.delete('/delete/{tank_id}', status_code=status.HTTP_204_NO_CONTENT, name='Delete tank')
def delete_tank(tank_id: int, tank_service: TanksService = Depends(), super_user_id: int = Depends(get_current_user_id)):
    return tank_service.delete_tank(tank_id)
