from datetime import datetime

from fastapi import APIRouter, Depends
from fastapi.responses import StreamingResponse

from src.services.files import FilesService
from src.services.users import get_current_user_id

router = APIRouter(
    prefix='/files',
    tags=['files']
)


@router.get('/download', name='Download csv-file')
def download(
        tank_id: int,
        product_id: int,
        date_start: datetime,
        date_end: datetime,
        files_service: FilesService = Depends(),
        super_user_id: int = Depends(get_current_user_id)
):
    report = files_service.download(tank_id, product_id, date_start, date_end)

    return StreamingResponse(
        report,
        media_type='text/csv',
        headers={
            'Content-Disposition': 'attachment; filename=report.csv'
        }
    )
