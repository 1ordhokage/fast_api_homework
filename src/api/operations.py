from fastapi import APIRouter, status, Depends

from src.models.schemas.operation.operation_request import OperationRequest, OperationUpdate
from src.models.schemas.operation.operation_response import OperationResponse, DetailOperationResponse
from src.services.operations import OperationsService
from src.services.users import get_current_user_id

router = APIRouter(
    prefix='/operations',
    tags=['operations'],
)


@router.post('/create', response_model=OperationResponse, status_code=status.HTTP_201_CREATED, name='Operation creation')
def create_operation(operation_schema: OperationRequest, operation_service: OperationsService = Depends(), super_user_id: int = Depends(get_current_user_id)):
    return operation_service.add_operation(operation_schema, super_user_id)


@router.get('/all', response_model=list[DetailOperationResponse], name='Get all operations (detailed)')
def create_operation(operation_service: OperationsService = Depends(), super_user_id: int = Depends(get_current_user_id)):
    return operation_service.get_all_operations()


@router.get('/get/{operation_id}', response_model=OperationResponse, name='Get operation by id')
def get_operation_by_id(operation_id: int, operation_service: OperationsService = Depends(), super_user_id: int = Depends(get_current_user_id)):
    return operation_service.get_operation_by_id(operation_id)


@router.get('/get_by_tank_id/{tank_id}', response_model=list[OperationResponse], name="Get all operations by tank_id")
def get_operation_by_tank_id(tank_id: int, operation_service: OperationsService = Depends(), super_user_id: int = Depends(get_current_user_id)):
    return operation_service.get_operations_by_tank(tank_id)


@router.put('/update/{operation_id}', response_model=OperationResponse, name='Update operation')
def update_operation(operation_id: int, operation_schema: OperationUpdate, operation_service: OperationsService = Depends(), super_user_id: int = Depends(get_current_user_id)):
    return operation_service.update_operation(operation_id, operation_schema, super_user_id)


@router.delete('/delete/{operation_id}', status_code=status.HTTP_204_NO_CONTENT, name='Delete operation')
def delete_operation(operation_id: int, operation_service: OperationsService = Depends(), super_user_id: int = Depends(get_current_user_id)):
    return operation_service.delete_operation(operation_id)
