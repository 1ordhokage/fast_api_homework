from fastapi import Depends, HTTPException, status
from sqlalchemy.orm import Session

from src.db.db import get_session
from src.models.product import Product
from src.models.schemas.product.product_request import ProductRequest


class ProductsService:
    def __init__(self, session: Session = Depends(get_session)) -> None:
        self.session = session

    def get_all_products(self) -> list[Product]:
        products = (
            self.session
            .query(Product)
            .order_by(
                Product.id.desc()
            )
            .all()
        )
        return products

    def get_product_by_id(self, product_id: int) -> Product:
        product = (
            self.session
            .query(Product)
            .filter(
                Product.id == product_id
            )
            .first()
        )
        if not product:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail="Product not found")
        return product
    
    def get_product_by_name(self, name: str) -> Product:
        product = (
            self.session
            .query(Product)
            .filter(
                Product.name == name
            )
            .first()
        )
        return product

    def add_product(self, product_schema: ProductRequest, super_user_id: int) -> Product:
        if self.get_product_by_name(product_schema.name):
            raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                                detail="Already exists")
        product = Product(
            name=product_schema.name,
            created_by=super_user_id,
        )
        self.session.add(product)
        self.session.commit()
        return product
    
    def update_product(self, product_id: int, product_schema: ProductRequest, super_user_id: int) -> Product:
        product = self.get_product_by_id(product_id)
        product.name = product_schema.name
        product.modified_by = super_user_id
        self.session.commit()
        return product

    def delete_product(self, product_id: int) -> None:
        product = self.get_product_by_id(product_id)
        self.session.delete(product)
        self.session.commit()
