import csv
from datetime import datetime
from io import StringIO
import pytz

from src.services.operations import OperationsService


class FilesService(OperationsService):
    def download(self, tank_id: int, product_id: int, date_start: datetime,
                 date_end: datetime):
        self.check_start_end(date_start, date_end)
        output = StringIO()
        writer = csv.DictWriter(output, fieldnames=[
            "id",
            "mass",
            "date_start",
            "date_end",
            "tank_id",
            "product_id",
            "created_at",
            "created_by",
            "modified_at",
            "modified_by"
        ])
        writer.writeheader()
        utc = pytz.UTC
        lst = self.get_operations_by_tank(tank_id)
        print(len(lst))
        operations = [operation for operation in lst
                      if operation.tank_id == product_id
                      and utc.localize(operation.date_start) >= date_start
                      and utc.localize(operation.date_end) <= date_end
                      ]
        print(len(operations))
        for operation in operations:
            writer.writerow(
                {
                    "id": operation.id,
                    "mass": operation.mass,
                    "date_start": operation.date_start,
                    "date_end": operation.date_end,
                    "tank_id": operation.tank_id,
                    "product_id": operation.product_id,
                    "created_at": operation.created_at,
                    "created_by": operation.created_by,
                    "modified_at": operation.modified_at,
                    "modified_by": operation.modified_by
                }
            )
        output.seek(0)
        return output
