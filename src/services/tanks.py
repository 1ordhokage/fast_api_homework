from fastapi import HTTPException, status

from src.services.products import ProductsService

from src.models.schemas.tank.tank_request import TankRequest, TankUpdate
from src.models.tank import Tank


class TanksService(ProductsService):
    @staticmethod
    def check_capacity(max_capacity: float, current_capacity: float) -> None:
        if max_capacity < current_capacity:
            raise HTTPException(
                status_code=status.HTTP_409_CONFLICT,
                detail='Current capacity cant be higher than max capacity'
            )

    def get_all_tanks(self) -> list[Tank]:
        tanks = (
            self.session
            .query(Tank)
            .order_by(
                Tank.id.desc()
            )
            .all()
        )
        return tanks
    
    def get_tank_by_id(self, tank_id: int) -> Tank:
        tank = (
            self.session
            .query(Tank)
            .filter(
                Tank.id == tank_id
            )
            .first()
        )
        if not tank:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail="tank not found")
        return tank

    def get_tank_by_name(self, name: str) -> Tank:
        tank = (
            self.session
            .query(Tank)
            .filter(
                Tank.name == name
            )
            .first()
        )
        return tank

    def add_tank(self, tank_schema: TankRequest, super_user_id: int) -> Tank:
        if self.get_tank_by_name(tank_schema.name):
            raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                                detail="Already exists")

        self.check_capacity(tank_schema.max_capacity, tank_schema.current_capacity)


        tank = Tank(
            name=tank_schema.name,
            max_capacity=tank_schema.max_capacity,
            current_capacity=tank_schema.current_capacity,
            product_id=self.get_product_by_id(tank_schema.product_id).id,
            created_by=super_user_id
        )
        self.session.add(tank)
        self.session.commit()
        return tank
    
    def update_tank(self, tank_id: int, tank_schema: TankUpdate, super_user_id: int) -> Tank:
        tank = self.get_tank_by_id(tank_id)
        was_modified = False
        for field, value in tank_schema:
            if value:
                if field == "product_id":
                    setattr(tank, field, self.get_product_by_id(value).id)
                else:
                    setattr(tank, field, value)
                was_modified = True
        self.check_capacity(tank.max_capacity, tank.current_capacity)
        if was_modified:
            tank.modified_by = super_user_id
        self.session.commit()
        return tank

    def update_capacity(self, tank_id: int, current_capacity: float, super_user_id: int) -> Tank:
        tank = self.get_tank_by_id(tank_id)
        self.check_capacity(tank.max_capacity, current_capacity)
        tank.current_capacity = current_capacity
        tank.modified_by = super_user_id
        self.session.commit()
        return tank

    def delete_tank(self, tank_id: int) -> None:
        tank = self.get_tank_by_id(tank_id)
        self.session.delete(tank)
        self.session.commit()
