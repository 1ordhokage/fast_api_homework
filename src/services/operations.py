from datetime import datetime

from fastapi import HTTPException, status

from src.models.operation import Operation
from src.models.schemas.operation.operation_request import OperationRequest, \
    OperationUpdate
from src.services.tanks import TanksService


class OperationsService(TanksService):
    @staticmethod
    def check_start_end(start: datetime, end: datetime) -> None:
        if end <= start:
            raise HTTPException(
                status_code=status.HTTP_409_CONFLICT,
                detail='Start datetime cant be bigger than end datetime'
            )

    def get_all_operations(self) -> list[Operation]:
        operations = (
            self.session
            .query(Operation)
            .order_by(
                Operation.id.desc()
            )
            .all()
        )
        detailed_operations = [

        ]
        return operations

    def get_operation_by_id(self, operation_id: int) -> Operation:
        operation = (
            self.session
            .query(Operation)
            .filter(
                Operation.id == operation_id
            )
            .first()
        )
        if not operation:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                                detail="Operation not found")
        return operation
    
    def add_operation(self, operation_schema: OperationRequest, super_user_id: int) -> Operation:
        self.check_start_end(operation_schema.date_start, operation_schema.date_end)
        operation = Operation(
            mass=operation_schema.mass,
            date_start=operation_schema.date_start,
            date_end=operation_schema.date_end,
            tank_id=self.get_tank_by_id(operation_schema.tank_id).id,
            product_id=self.get_product_by_id(operation_schema.product_id).id,
            created_by=super_user_id
        )
        self.session.add(operation)
        self.session.commit()
        return operation

    def update_operation(self, operation_id: int, operation_schema: OperationUpdate, super_user_id: int) -> Operation:
        operation = self.get_operation_by_id(operation_id)
        was_modified = False
        for field, value in operation_schema:
            if value:
                if field == "product_id":
                    setattr(operation, field, self.get_product_by_id(value).id)
                elif field == "tank_id":
                    setattr(operation, field, self.get_tank_by_id(value).id)
                else:
                    setattr(operation, field, value)
                was_modified = True
        self.check_start_end(operation_schema.date_start, operation_schema.date_end)
        if was_modified:
            operation.modified_by = super_user_id
        self.session.commit()
        return operation

    def delete_operation(self, operation_id: int) -> None:
        operation = self.get_operation_by_id(operation_id)
        self.session.delete(operation)
        self.session.commit()

    def get_operations_by_tank(self, tank_id: int) -> list[Operation]:
        tank = self.get_tank_by_id(tank_id)
        return tank.operations

    def get_interval(self):
        pass
