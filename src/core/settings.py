from pydantic import BaseSettings


class Settings(BaseSettings):
    db_connection_string: str

    jwt_secret: str
    jwt_algorithm: str
    jwt_expires_seconds: int

    admin_login: str
    admin_password: str

    class Config:
        env_file = '../.env'


settings = Settings()
