from datetime import datetime

from sqlalchemy import Column, DateTime, Float, Integer, String, ForeignKey

from src.models.base import Base


class Tank(Base):
    __tablename__ = 'tanks'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    max_capacity = Column(Float)
    current_capacity = Column(Float)

    product_id = Column(Integer, ForeignKey('products.id'), index=True)
    created_at = Column(DateTime, default=datetime.now)
    created_by = Column(Integer, ForeignKey('users.id'))
    modified_at = Column(DateTime, onupdate=datetime.now, nullable=True)
    modified_by = Column(Integer, ForeignKey('users.id'), nullable=True)
