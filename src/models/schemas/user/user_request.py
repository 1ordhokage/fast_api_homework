from pydantic import BaseModel

from src.models.schemas.user.roles import Roles


class UserRequest(BaseModel):
    username: str
    password_text: str
    role: Roles = Roles.viewer

    class Config:
        use_enum_values = True


class UserUpdate(BaseModel):
    username: str | None
    password_text: str | None
    role: Roles | None

    class Config:
        use_enum_values = True
