from enum import Enum


class Roles(Enum):
    admin = 'admin'
    viewer = 'viewer'
