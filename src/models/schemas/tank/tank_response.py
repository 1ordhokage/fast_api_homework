from pydantic import BaseModel


class TankResponse(BaseModel):
    id: int
    name: str
    max_capacity: float
    current_capacity: float
    product_id: int

    class Config:
        orm_mode = True
