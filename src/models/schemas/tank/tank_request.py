from pydantic import BaseModel


class TankRequest(BaseModel):
    name: str
    max_capacity: float
    current_capacity: float
    product_id: int


class TankUpdate(BaseModel):
    name: str | None = None
    max_capacity: float | None = None
    current_capacity: float | None = None
    product_id: int | None = None
