from datetime import datetime

from pydantic import BaseModel

from src.models.schemas.product.product_response import ProductResponse
from src.models.schemas.tank.tank_response import TankResponse


class OperationResponse(BaseModel):
    id: int
    mass: float
    date_start: datetime
    date_end: datetime
    tank_id: int
    product_id: int

    class Config:
        orm_mode = True


class DetailOperationResponse(BaseModel):
    id: int
    mass: float
    date_start: datetime
    date_end: datetime
    tank: TankResponse
    product: ProductResponse

    class Config:
        orm_mode = True
