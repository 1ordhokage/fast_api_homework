from datetime import datetime

from pydantic import BaseModel


class OperationRequest(BaseModel):
    mass: float
    date_start: datetime
    date_end: datetime
    tank_id: int
    product_id: int


class OperationUpdate(BaseModel):
    mass: float | None = None
    date_start: datetime | None = None
    date_end: datetime | None = None
    tank_id: int | None = None
    product_id: int | None = None
