from datetime import datetime

from sqlalchemy import Column, DateTime, Integer, String, ForeignKey

from src.models.base import Base


class Product(Base):
    __tablename__ = 'products'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    created_at = Column(DateTime, default=datetime.now)
    created_by = Column(Integer, ForeignKey('users.id'))
    modified_at = Column(DateTime, onupdate=datetime.now, nullable=True)
    modified_by = Column(Integer, ForeignKey('users.id'), nullable=True)
