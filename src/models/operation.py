from datetime import datetime

from sqlalchemy import Column, DateTime, Float, ForeignKey, Integer
from sqlalchemy.orm import relationship

from src.models.base import Base


class Operation(Base):
    __tablename__ = 'operations'
    id = Column(Integer, primary_key=True)
    mass = Column(Float)
    date_start = Column(DateTime, nullable=True)
    date_end = Column(DateTime, nullable=True)
    tank_id = Column(Integer, ForeignKey('tanks.id'), index=True)
    product_id = Column(Integer, ForeignKey('products.id'), index=True)
    created_at = Column(DateTime, default=datetime.now)
    created_by = Column(Integer, ForeignKey('users.id'))
    modified_at = Column(DateTime, onupdate=datetime.now, nullable=True)
    modified_by = Column(Integer, ForeignKey('users.id'), nullable=True)

    product = relationship('Product', backref='operations')
    tank = relationship('Tank', backref='operations')
